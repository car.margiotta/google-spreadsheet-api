#lang scribble/manual

@title{google-spreadsheet-api}
@author[(author+email @elem{Carmine Margiotta}
                      "car.margiotta@icloud.com")]

@defmodule[google-spreadsheet-api]

A simple interface for Google Sheets API, supporting authentication.

You will need a Google Service Account:
https://support.google.com/a/answer/7378726?hl=en
And the relative JSON containing the private key.

@section{Initializing the connection}
@defproc[(init-api [#:json-path json-path path-string?])
         api-connection?]{
 This procedure parses the JSON (at @racket[json-path]) provided by Google and uses the private key for signing requests. The produced api-connection will be needed by every other procedure.

 Examples:
 @codeblock|{
                                              > (require google-spreadsheet-api)
                                              > (define conn (init-api #:json-path "data/google-auth.json"))
                                              }|
}

@section{Reading a spreadsheet}
@defproc[(get-cells [conn api-connection?]
                    [sheet (or/c valid-sheet-id? valid-sheet-url?)]
                    [coordinates valid-range?]) 
         (listof (listof (or/c number? string?)))]{
 This procedure downloads cells at the given coordinates and places them in a list of lists (a list of rows). The @racket[conn] must be initialized with @racket[init-api].

 Examples:
 @codeblock|{
             > (require google-spreadsheet-api)
             > (define conn (init-api #:json-path "data/google-auth.json"))
             > (get-cells conn "1Gvw2x2ygZ5YYcGCxDiq4ZaD-1qdvX31NkX1ZkgmFYvE" "A2:C3")
             '(("snacks", 540, 10.1), ("lasagna", 140, 15.5))
           }|
}

@section{Inserting a row}
@defproc[(insert-row [conn api-connection?]
                     [sheet (or/c valid-sheet-id? valid-sheet-url?)]
                     [range valid-range?]
                     [values (listof (or/c number? string?))])
         any]{
              This procedure appends a new row to the subtable delimited by @racket[range], for example @racket["A2:D"].

              Examples:
              @codeblock|{
                          > (require google-spreadsheet-api)
                          > (define conn (init-api #:json-path "data/google-auth.json"))
                          > (insert-row conn "1Gvw2x2ygZ5YYcGCxDiq4ZaD-1qdvX31NkX1ZkgmFYvE" "A2:D")
                          }|
              }

@section{Modifying a row}
@defproc[(update-row [conn api-connection?]
                     [sheet (or/c valid-sheet-id? valid-sheet-url?)]
                     [coordinates valid-range?]
                     [values (listof (or/c number? string?))])
         any]{
              This procedure modifies an existing row identified by the given @racket[coordinates].

              Examples:
              @codeblock|{
                          > (require google-spreadsheet-api)
                          > (define conn (init-api #:json-path "data/google-auth.json"))
                          > (update-row conn "1Gvw2x2ygZ5YYcGCxDiq4ZaD-1qdvX31NkX1ZkgmFYvE" "A2:C2" '("lasagna", 520, 100))
                          }|
              }
                     
@section{"Predicates"}
@defproc[(valid-sheet-id? [id any/c]) boolean?]{A valid sheet ID is a string made of an arbitrary number of alphanumeric characters, a - and another arbitrary number of alphanumeric characters.}
@defproc[(valid-sheet-url? [url any/c]) boolean?]{A valid sheet URL is a string made of @racket["https://docs.google.com/spreadsheets/d/"] and a @racket[valid-sheet-id?].}
@defproc[(valid-range? [range any/c]) boolean?]{A valid range is a string that matches the regular expression "^[A-Z]{1}[0-9]*:[A-Z]{1}[0-9]*$"}
